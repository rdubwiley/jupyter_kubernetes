FROM python:3

RUN apt-get update && pip install jupyter numpy

WORKDIR /home

RUN mkdir jupyter

WORKDIR ./jupyter

CMD jupyter notebook --port 8000 --ip=0.0.0.0 --allow-root
